# MODISAutoDecoder
Powered by MrFentazis

Модуль для обработки MODIS для LorettDecoder. Использование как автономное CLI приложение описано ниже:
Подробное описание будет позже..

## Установка:

### windows:
Зависимости:
- Устанавливаем Python >3.11
- Устанавливаем SatDump (https://github.com/SatDump/SatDump/releases или https://disk.yandex.ru/d/hGfBN-09fhVAYw)
  Можно использовать Nightly Builds (Скачиваем архив SatDump-Windows_x64_MSVC.zip)
- Распаковываем содержимое в С://lorett//SatDump (Либо в другую, но потом потребуется поменять путь в config.json)
- Опционально запускаем файл volk_profile.exe, это займет достаточно много времени, но ускорит обработку данных в дальнейшем.
- Устанавливаем fpf (https://github.com/alxndrsh/fpf/releases/tag/0.9)
  Скачиваем архив fpf_windows_rev09.zip
- Распаковываем содержимое ВЛОЖЕННОЙ папки в С://lorett//fpf (Либо в другую, но потом потребуется поменять путь в config.json)

MODISAutoDecoder:
```
git clone https://gitlab.com/lpmrfentazis/MODISAutoDecoder.git
python -m venv .env
.env/Scripts/activate.bat
pip install -r requirements.txt
```



Перед использованием необходимо настроить приложение:
Параметры указываются в config.json, если файла с настройками нет - при первом запуске будет создан с параметрами по умолчанию.

- ⚙  satDumpPath  ⚙ (Путь к satDump)
- ⚙  fpfPath  ⚙ (Путь к fpf)
- ⚙  dataPath     ⚙ (Пути к папкам с данными, через запятую)
- ⚙  iniPath     ⚙ (Путь к файлу конфигурации для fpf, по умолчанию хранится в ./ini в папке MODISAutoDecoder)
- ⚙  keepGood   ⚙ (Сохранять ли исходные файлы если данные есть)
- ⚙  keepBad   ⚙ (Сохранять ли исходные файлы если данных нет)


Лог работы программы записывается в файл рядом.

Спутники, доступные по умолчанию:
- 🛰  TERRA  🛰
- 🛰  AQUA   🛰
