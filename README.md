# MODISAutoDecoder
Powered by MrFentazis

MODIS processing module for LorettDecoder. Use as a standalone CLI application is described below:
Detailed description to come later...

### Installation:

### windows:
Dependencies:
- Install Python >3.11
- Install SatDump (https://github.com/SatDump/SatDump/releases or https://disk.yandex.ru/d/hGfBN-09fhVAYw)
  You can use Nightly Builds (Download SatDump-Windows_x64_MSVC.zip).
- Extract the contents to C://lorett//SatDump (Or to another, but then you will need to change the path in config.json).
- Optionally run the volk_profile.exe file, it will take quite a long time, but will speed up data processing later.
- Install fpf (https://github.com/alxndrsh/fpf/releases/tag/0.9)
  Download the archive fpf_windows_rev09.zip
- Extract the contents of the INPUT folder to C://lorett/fpf (Or to another folder, but then you will need to change the path in config.json).

MODISAutoDecoder:
```
git clone https://gitlab.com/lpmrfentazis/MODISAutoDecoder.git
python -m venv .env
.env/Scripts/activate.bat.
pip install -r requirements.txt
```



The application must be configured before use:
Parameters are specified in config.json, if there is no customization file - it will be created with default parameters on first run.

- ⚙ satDumpPath ⚙ (Path to satDump)
- ⚙ fpfPath ⚙ (Path to fpf)
- ⚙ dataPath ⚙ (Paths to data folders, separated by commas)
- ⚙ iniPath ⚙ (Path to configuration file for fpf, by default stored in ./ini in MODISAutoDecoder folder)
- ⚙ keepGood ⚙ (Whether to save source files if data is available)
- ⚙ keepBad ⚙ (Whether to keep the source files if there is no data)


The log of the program operation is written to a file nearby.

Satellites available by default:
- 🛰 TERRA 🛰
- 🛰 AQUA 🛰