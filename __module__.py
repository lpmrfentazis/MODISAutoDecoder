from MODISAutoDecoder import MODISAutoDecoder, __version__ as ver
from Config import MODISDecoderConfig, MODISDecoderConfigLoader
from Logger import Logger

decoder = MODISAutoDecoder
config = MODISDecoderConfig
configLoader = MODISDecoderConfigLoader
logger = Logger

__version__ = ver
