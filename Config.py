from pathlib import Path
from json import dump, load
from dataclasses import dataclass
from pprint import pformat
from sys import platform

from Logger import Logger, loggingLevels
from verboselogs import VerboseLogger, VERBOSE


config = {
    "satDumpPath": "C:\\Lorett\\SatDump" if platform == "win32" else "~/lorett/satdump",
    "fpfPath": "C:\\Lorett\\fpf" if platform == "win32" else "~/lorett/fpf",
    "iniPath": "C:\\Lorett\\MODISAutoDecoder\\ini" if platform == "win32" else "~/lorett/MODISAutoDecoder/ini",
    "dataPath": ["C:\\Lorett\\data"] if platform == "win32" else ["~/lorett/data"],
    "logPath": "",
    
    "satellites": {
        "TERRA": {"pipeline": "terra_db", "ini": "dat2cadu_terra_aqua.ini", "mainOutput": "MODIS", "minSize": 200},
        "AQUA": {"pipeline": "aqua_db", "ini": "dat2cadu_terra_aqua.ini", "mainOutput": "MODIS", "minSize": 200},
    }, 
    
    "loggingLevel": "verbose",
    "checkDirInterval": 1,
    
    "spaceLimit": 30,
    "noFreeSpaceTimeout": 0,

    "keepGood": False,
    "keepBad": False
}


@dataclass
class MODISDecoderConfig:
    satDumpPath: Path
    dataPath: Path
    
    satellites: dict
    
    loggingLevel: int
    checkDirInterval: int
    
    keepGood: bool
    keepBad: bool
    
    spaceLimit: int
    noFreeSpaceTimeout: int
    
    logger: VerboseLogger
    
    
class MODISDecoderConfigLoader(MODISDecoderConfig):
    def __init__(self, path: str) -> None:
        self.__path = Path(path)
        
        if not self.__path.exists():
            self.loggingLevel = loggingLevels[config["loggingLevel"]]
            self.logPath = Path(config["logPath"])
            
            self.logPath.mkdir(parents=True, exist_ok=True)
            
            self.logger = Logger("decoder", self.logPath, self.loggingLevel)
            self.logger.error(f"File {self.__path} is not exists")
            self.logger.info("Try use default config")  
            
            self.satDumpPath = Path(config["satDumpPath"])
            self.fpfPath = Path(config["fpfPath"])
            self.iniPath = Path(config["iniPath"])
            
            self.dataPath = [Path(i) for i in config["dataPath"]]
            for i in self.dataPath:
                i.mkdir(parents=True, exist_ok=True)
            
            self.satellites = config["satellites"]
            
            self.checkDirInterval = config["checkDirInterval"]
            
            self.spaceLimit = config["spaceLimit"]
            self.noFreeSpaceTimeout = config["noFreeSpaceTimeout"] if config['noFreeSpaceTimeout'] > 0 else None
            
            self.keepBad = config["keepBad"]
            self.keepGood = config["keepGood"]
            
            self.logger.verbose("\n" + pformat(config, indent=4, width=120, sort_dicts=False))
            
            self.save()
            
        else:
            with open(self.__path, "r") as f:
                fromJson = load(f)
                
                missed = tuple(i for i in config.keys() if i not in fromJson)
                                              
                self.loggingLevel = loggingLevels[fromJson.get("loggingLevel", config["loggingLevel"])]
                self.logPath = Path(fromJson.get("logPath", config["logPath"]))
                
                self.logger = Logger("decoder", self.logPath, self.loggingLevel)
                
                for i in missed:
                    self.logger.warning(f"{i} key missed in config. Try use default: {config[i]}")
                    fromJson[i] = config[i]
                                
                self.satDumpPath = Path(fromJson["satDumpPath"])
                self.fpfPath = Path(fromJson["fpfPath"])
                self.iniPath = Path(fromJson["iniPath"])
                
                self.dataPath = [Path(i) for i in fromJson["dataPath"]]
                
                for i in self.dataPath:
                    i.mkdir(parents=True, exist_ok=True)
                
                self.satellites = fromJson["satellites"]
                
                self.spaceLimit = fromJson["spaceLimit"]
                self.noFreeSpaceTimeout = fromJson["noFreeSpaceTimeout"] if fromJson["noFreeSpaceTimeout"] > 0 else None
                
                self.checkDirInterval = fromJson["checkDirInterval"]
                self.keepBad = fromJson["keepBad"]
                self.keepGood = fromJson["keepGood"]
                
                if len(missed):
                    self.save()  
                
                self.logger.verbose("\n" + pformat(fromJson, indent=4, width=120, sort_dicts=False))                          

        for i in self.dataPath:
            if not i.exists():
                self.logger.critical(f"{i} is not exists")
                raise FileNotFoundError(f"DataPath directory not found: {i}")

        if not self.satDumpPath.exists():
            self.logger.critical(f"{self.satDumpPath} is not exists")
            raise FileNotFoundError(f"SatDumpPath directory not found: {i}")
        
        if not self.fpfPath.exists():
            self.logger.critical(f"{self.fpfPath} is not exists")
            raise FileNotFoundError(f"FpfPath directory not found: {i}")
                
        if not self.iniPath.exists():
            self.logger.critical(f"{self.iniPath} is not exists")
            raise FileNotFoundError(f"iniPath directory not found: {i}")
        
        for sat in self.satellites.values():
            if not (self.iniPath / sat["ini"]).exists():
                self.logger.critical(f"{(self.iniPath / sat['ini'])} is not exists")
                raise FileNotFoundError(f"ini File not found: {sat}")
        
        # Platform check
        if platform == "win32":
            self.satDump = "satdump.exe"
            self.fpf = "fpf.exe"
        else:
            self.satDump = "satdump"
            self.fpf = "fpf"

        if not (self.fpfPath / self.fpf).exists():
            self.logger.critical("Fpf executable file is not found. Exit")
            raise FileNotFoundError(f"Fpf executable file is not found: {self.fpfPath / self.fpf}")
        
        if not (self.satDumpPath / self.satDump).exists():
            self.logger.critical("SatDump executable file is not found. Exit")
            raise FileNotFoundError(f"SatDump executable file is not found: {self.satDumpPath / self.satDump}")
        
    def save(self) -> None:
        with open(self.__path, "w") as f:
            self.logger.info(f"Save config to {self.__path}")
            
            dump({
                "satDumpPath": str(self.satDumpPath),
                "fpfPath": str(self.fpfPath),
                "iniPath": str(self.iniPath),
                "dataPath": [ str(i) for i in self.dataPath],
                "logPath": str(self.logPath),
                
                "satellites": self.satellites,
    
                "loggingLevel": loggingLevels[self.loggingLevel],
                "spaceLimit": self.spaceLimit,
                "noFreeSpaceTimeout": 0 if self.noFreeSpaceTimeout is None else self.noFreeSpaceTimeout,
                "checkDirInterval": self.checkDirInterval,

                "keepGood": self.keepGood,
                "keepBad": self.keepBad
            }, f, indent=4, sort_keys=False)